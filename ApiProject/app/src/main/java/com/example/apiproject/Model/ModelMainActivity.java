package com.example.apiproject.Model;


import com.example.apiproject.Interfaces.InterfacePrincipal;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ModelMainActivity implements InterfacePrincipal.Model {
    InterfacePrincipal.Presenter presenter;
    private Retrofit retrofit;

    public ModelMainActivity(InterfacePrincipal.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void Adapter() {
        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.freetogame.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InterfacePrincipal interfacePrincipal = retrofit.create(InterfacePrincipal.class);
        Call<ArrayList<Atributos>> Call = interfacePrincipal.getAtributos();
        System.out.println("url " + Call.request().url());
        Call.enqueue(new Callback<ArrayList<Atributos>>() {
            @Override
            public void onResponse(retrofit2.Call<ArrayList<Atributos>> call, Response<ArrayList<Atributos>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Atributos> elements = response.body();
                    presenter.showResultadoView(elements);
                }
            }

            @Override
            public void onFailure(retrofit2.Call<ArrayList<Atributos>> call, Throwable t) {

            }
        });
    }

    @Override
    public void AdapterBusqueda(String plataforma) {
        String a = "shooter";
        String b = "alfabetico";
        String c = "plataforma";
        String d = "todos";
        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.freetogame.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        InterfacePrincipal interfacePrincipal = retrofit.create(InterfacePrincipal.class);
        Call<ArrayList<Atributos>> Call;
        if (plataforma.equals(c)) {
            Call = interfacePrincipal.getPlataforma();
            Call.enqueue(new Callback<ArrayList<Atributos>>() {
                @Override
                public void onResponse(retrofit2.Call<ArrayList<Atributos>> call, Response<ArrayList<Atributos>> response) {
                    if (response.isSuccessful()) {
                        ArrayList<Atributos> elements = response.body();
                        presenter.showResultadoView(elements);
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ArrayList<Atributos>> call, Throwable t) {
                }
            });
        } else if (plataforma.equals(a)) {
            Call = interfacePrincipal.getTipo();
            Call.enqueue(new Callback<ArrayList<Atributos>>() {
                @Override
                public void onResponse(retrofit2.Call<ArrayList<Atributos>> call, Response<ArrayList<Atributos>> response) {
                    if (response.isSuccessful()) {
                        ArrayList<Atributos> elements = response.body();
                        presenter.showResultadoView(elements);
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ArrayList<Atributos>> call, Throwable t) {
                }
            });
        } else if (plataforma.equals(b)) {
            Call = interfacePrincipal.getAlfabetico();
            Call.enqueue(new Callback<ArrayList<Atributos>>() {
                @Override
                public void onResponse(retrofit2.Call<ArrayList<Atributos>> call, Response<ArrayList<Atributos>> response) {
                    if (response.isSuccessful()) {
                        ArrayList<Atributos> elements = response.body();
                        presenter.showResultadoView(elements);
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ArrayList<Atributos>> call, Throwable t) {
                }
            });
        } else if (plataforma.equals(d)) {
            Call = interfacePrincipal.getAtributos();
            Call.enqueue(new Callback<ArrayList<Atributos>>() {
                @Override
                public void onResponse(retrofit2.Call<ArrayList<Atributos>> call, Response<ArrayList<Atributos>> response) {
                    if (response.isSuccessful()) {
                        ArrayList<Atributos> elements = response.body();
                        presenter.showResultadoView(elements);
                    }
                }

                @Override
                public void onFailure(retrofit2.Call<ArrayList<Atributos>> call, Throwable t) {
                }
            });
        }
    }
}




