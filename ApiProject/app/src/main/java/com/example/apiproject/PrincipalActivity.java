package com.example.apiproject;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apiproject.Interfaces.InterfacePrincipal;
import com.example.apiproject.LlamadoApi.LlamadoApi;
import com.example.apiproject.Model.Atributos;
import com.example.apiproject.Model.ListAdapter;
import com.example.apiproject.Model.ListAdapterImg;

import java.util.ArrayList;


public class PrincipalActivity extends AppCompatActivity implements InterfacePrincipal.View, SearchView.OnQueryTextListener {

    InterfacePrincipal.Presenter presenter;
    private RecyclerView recycler_view_card_api;
    ArrayList<Atributos> elements;
    private AppCompatButton button_plataforma, button_shooter, button_alfabetico, button_all;
    RecyclerView.LayoutManager layoutManager;
    ListAdapterImg listAdapterImg;
    SearchView search_view;
    ListAdapter listAdapter;
    int []arr={R.drawable.apexl,R.drawable.fortniteuno,R.drawable.lol,R.drawable.smite,R.drawable.wzp,R.drawable.pubg};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        presenter = new LlamadoApi(this);
        recycler_view_card_api = findViewById(R.id.recycler_view_card_api);
        button_plataforma = findViewById(R.id.button_plataforma);
        button_shooter = findViewById(R.id.button_shooter);
        button_alfabetico = findViewById(R.id.button_alfabetico);
        search_view = findViewById(R.id.search_view);
        button_all = findViewById(R.id.button_all);

        RecyclerView recyclerView = findViewById(R.id.recycler_view_card);
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(manager);
        listAdapterImg = new ListAdapterImg(arr,this);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(listAdapterImg);
        resultadoBottonView();
        resultadoView();
        search_view.setOnQueryTextListener(this);
    }
    @Override
    public void resultadoView() {
        elements = new ArrayList<>();
        presenter.LlamarModelAdapter();
    }

    @Override
    public void resultadoBottonView() {
        elements = new ArrayList<>();
        button_plataforma.setOnClickListener(view -> {
            String plataforma = "plataforma";
            presenter.LlamarModelBusqueda(plataforma);
            button_plataforma.setBackgroundResource(R.drawable.disenhobuttonaccionados);
            button_alfabetico.setBackgroundResource(R.drawable.disenhobuttons);
            button_shooter.setBackgroundResource(R.drawable.disenhobuttons);
            button_all.setBackgroundResource(R.drawable.disenhobuttons);
        });
        button_shooter.setOnClickListener(view -> {
            String shooter = "shooter";
            presenter.LlamarModelBusqueda(shooter);
            button_plataforma.setBackgroundResource(R.drawable.disenhobuttons);
            button_alfabetico.setBackgroundResource(R.drawable.disenhobuttons);
            button_shooter.setBackgroundResource(R.drawable.disenhobuttonaccionados);
            button_all.setBackgroundResource(R.drawable.disenhobuttons);
        });
        button_alfabetico.setOnClickListener(view -> {
            String alfabetico ="alfabetico";
            presenter.LlamarModelBusqueda(alfabetico);
            button_plataforma.setBackgroundResource(R.drawable.disenhobuttons);
            button_alfabetico.setBackgroundResource(R.drawable.disenhobuttonaccionados);
            button_shooter.setBackgroundResource(R.drawable.disenhobuttons);
            button_all.setBackgroundResource(R.drawable.disenhobuttons);
        });
        button_all.setOnClickListener(view -> {
            String all = "todos";
            presenter.LlamarModelBusqueda(all);
            button_plataforma.setBackgroundResource(R.drawable.disenhobuttons);
            button_alfabetico.setBackgroundResource(R.drawable.disenhobuttons);
            button_shooter.setBackgroundResource(R.drawable.disenhobuttons);
            button_all.setBackgroundResource(R.drawable.disenhobuttonaccionados);
        });
    }

    @Override
    public void showResultadoArrelgo(ArrayList<Atributos> elements) {
        RecyclerView recyclerView = findViewById(R.id.recycler_view_card_api);
        listAdapter = new ListAdapter(elements, this);
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setHasFixedSize(false);
        recyclerView.setAdapter(listAdapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.filtrado(newText);
        return false;
    }
}