package com.example.apiproject.Interfaces;

import com.example.apiproject.Model.Atributos;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface InterfacePrincipal {

    @GET("games")
    Call<ArrayList<Atributos>> getAtributos();

    @GET("games?platform=pc")
    Call<ArrayList<Atributos>> getPlataforma();

    @GET("games?category=shooter")
    Call<ArrayList<Atributos>> getTipo();

    @GET("games?sort-by=alphabetical")
    Call<ArrayList<Atributos>> getAlfabetico();

    interface View {

        void resultadoView();

        void resultadoBottonView();

        void showResultadoArrelgo(ArrayList<Atributos> elements);

    }

    interface Presenter {

        void LlamarModelBusqueda(String plataforma);

        void LlamarModelAdapter();

        void showResultadoView(ArrayList<Atributos> elements);

    }

    interface Model {



        void AdapterBusqueda(String plataforma);

        void Adapter();
    }
}
