package com.example.apiproject.LlamadoApi;

import android.app.Activity;

import com.example.apiproject.Interfaces.InterfacePrincipal;
import com.example.apiproject.Model.Atributos;
import com.example.apiproject.Model.ModelMainActivity;

import java.util.ArrayList;


public class LlamadoApi extends Activity implements InterfacePrincipal.Presenter {
    InterfacePrincipal.View view;
    InterfacePrincipal.Model model;

    public LlamadoApi(InterfacePrincipal.View view) {
        this.view = view;
        this.model = new ModelMainActivity(this);
    }

    @Override
    public void LlamarModelBusqueda(String plataforma) {
        model.AdapterBusqueda(plataforma);
    }

    @Override
    public void LlamarModelAdapter() {
        model.Adapter();
    }

    @Override
    public void showResultadoView(ArrayList<Atributos> elements) {
        view.showResultadoArrelgo(elements);
    }
}
