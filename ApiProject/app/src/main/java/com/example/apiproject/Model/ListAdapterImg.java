package com.example.apiproject.Model;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.apiproject.Interfaces.InterfacePrincipal;
import com.example.apiproject.R;

import java.util.ArrayList;

public class ListAdapterImg extends RecyclerView.Adapter<ListAdapterImg.ViewHolder> {
    int[] arr;
    private Context context;

    public ListAdapterImg(int[] arr, Context context) {
        this.arr = arr;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_image_scorll, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Glide.with(context).load(arr[position]).into(holder.imageViewGamesScroll);
    }

    @Override
    public int getItemCount() {
        return arr.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageViewGamesScroll;
        TextView text;

        ViewHolder(View itemView) {
            super(itemView);
            imageViewGamesScroll = itemView.findViewById(R.id.imageViewGamesScroll);
            text = itemView.findViewById(R.id.text);
        }
    }
}
