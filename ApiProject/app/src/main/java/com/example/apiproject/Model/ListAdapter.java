package com.example.apiproject.Model;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.apiproject.R;
import com.example.apiproject.SegundaActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private ArrayList<Atributos> mData;
    private ArrayList<Atributos> listabusqueda;
    private LayoutInflater mInflater;
    private Context context;

    public ListAdapter(ArrayList<Atributos> itemList, Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        this.mData = itemList;
        listabusqueda = new ArrayList<>();
        listabusqueda.addAll(mData);

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_card_games, null);
        //view.setOnClickListener(this);
        return new ListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindData(mData.get(position));
        Atributos p = mData.get(position);
        Glide.with(context)
                .load("https://www.freetogame.com/g/" + p.getId() + "/thumbnail.jpg")
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.thumbnail);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Atributos atributos = mData.get(position);
                Intent intent = new Intent(context, SegundaActivity.class);
                intent.putExtra("elementos1", (Serializable) atributos);
                context.startActivity(intent);
            }
        });
    }

    public void filtrado(String textbusqueda) {
        int longitud = textbusqueda.length();
        if (longitud == 0) {
            mData.clear();
            mData.addAll(listabusqueda);
        } else {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                List<Atributos> collecion = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    collecion = mData.stream().filter(i -> i.getTitle().contains(textbusqueda)).collect(Collectors.toList());
                }
                mData.clear();
                mData.addAll(collecion);
            } else {
                for (Atributos t : listabusqueda) {
                    if (t.getTitle().contains(textbusqueda)) {
                        mData.add(t);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView id, title, short_description, game_url, genre, platform, publisher, developer, release_date, freetogame_profile_url;
        ImageView thumbnail;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.Text_View_Game);
            thumbnail = itemView.findViewById(R.id.ImageViewGames);
        }

        void bindData(final Atributos item) {
            title.setText(item.getTitle());
        }
    }
}
