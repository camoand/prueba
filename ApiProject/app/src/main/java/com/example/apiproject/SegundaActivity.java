package com.example.apiproject;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.apiproject.Model.Atributos;


public class SegundaActivity extends AppCompatActivity {

    ImageView image_view_segundo_principal, image_view_segundo_mini;
    TextView text_view_descripcion, text_view_titulo_juego, text_view_tipojuego, text_view_fecha_lanzamiento, text_view_plataforma,
            text_View_desarrollado, text_View_publicado, text_View_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        Atributos atributos = (Atributos) getIntent().getSerializableExtra("elementos");

        image_view_segundo_mini = findViewById(R.id.image_view_segundo_mini);
        image_view_segundo_principal = findViewById(R.id.image_view_segundo_principal);
        text_view_descripcion = findViewById(R.id.text_view_descripcion);
        text_view_titulo_juego = findViewById(R.id.text_view_titulo_juego);
        text_view_tipojuego = findViewById(R.id.text_view_tipojuego);
        text_view_fecha_lanzamiento = findViewById(R.id.text_view_fecha_lanzamiento);
        text_view_plataforma = findViewById(R.id.text_view_plataforma);
        text_View_desarrollado = findViewById(R.id.text_View_desarrollado);
        text_View_publicado = findViewById(R.id.text_View_publicado);
        text_View_url = findViewById(R.id.text_View_url);

        text_view_descripcion.setText(atributos.getShort_description());
        text_View_url.setText(atributos.getGame_url());
        text_View_publicado.setText(atributos.getPublisher());
        text_View_desarrollado.setText(atributos.getDeveloper());
        text_view_plataforma.setText(atributos.getPlatform());
        text_view_fecha_lanzamiento.setText(atributos.getRelease_date());
        text_view_tipojuego.setText(atributos.getGenre());
        text_view_titulo_juego.setText(atributos.getTitle());

        Glide.with(this)
                .load(atributos.getThumbnail())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image_view_segundo_principal);
        Glide.with(this)
                .load(atributos.getThumbnail())
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(image_view_segundo_mini);
    }
}